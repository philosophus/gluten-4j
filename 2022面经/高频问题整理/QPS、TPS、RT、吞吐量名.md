## QPS (Queries Per Second) - 每秒查询数

   QPS 是一个衡量系统或应用程序处理查询或请求的速度的度量单位。它表示在**一秒内系统能够处理的查询或请求的数量**。对于数据库、Web服务器或其他需要处理请求的系统，QPS是一个关键性能指标。更高的QPS通常表示系统能够处理更多的请求。

## TPS (Transactions Per Second) - 每秒事务数

   TPS 是一个度量单位，用于描述在**一秒内完成的事务或操作的数量。事务可以是任何一系列相关的操作，例如数据库事务、支付处理或网络事务**。TPS通常用于评估系统的事务处理能力。与QPS类似，更高的TPS表示系统能够处理更多的事务。

## RT (Response Time) - 响应时间

   响应时间是系统或应用程序响应请求所需的时间。它通常以毫秒为单位表示，并衡量从发送请求到接收到响应的时间。较低的响应时间通常被认为是更好的性能指标，因为它表示系统能够更快地处理请求。响应时间是用户体验的重要组成部分，特别是在Web应用程序和移动应用程序中。

   当提到 RT 时，通常会根据上下文来理解其具体含义。一般来说是讨论时间内多个请求或事务的响应时间的平均值。

## Throughput - 吞吐量

   吞吐量是指系统在一定时间内成功处理的请求或事务的总数。它是一个综合性能度量，考虑了系统的处理速度和容量。高吞吐量表示系统能够有效地处理大量请求或事务，而不会出现性能瓶颈。吞吐量通常以每秒处理的请求或事务数量来衡量。

## PV (Page View) - 页面浏览

   页面访问量。

   PV 与访问者数量之间存在一定的关系。如果每个访问者浏览多个页面，PV 数量可能会比实际的访问者数量大得多。因此，在进行网站分析时，通常还会关注其他指标，如独立访客数、会话数和平均页面浏览量，以获得更全面的了解。

## 通用公式

```
QPS = 并发数 / 平均响应时间
并发数 = QPS * 平均响应时间
```

## 需要的服务器数量

根据二八定律，每天80%请求发生在20%的时间内。

```
(PV * 80%) / (每天秒数) = 峰值时间QPS
峰值时间QPS / 单台机器QPS = 需要机器的数量
```
