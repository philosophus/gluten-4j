---
highlight: xcode
theme: github
---
# 启动流程
1. 从`main`找到`run()`方法，在执行`run()`方法之前new一个`SpringApplication`对象
2. 进入`run()`方法，创建应用监听器`SpringApplicationRunListeners`开始监听
3. 加载SpringBoot配置环境(`ConfigurableEnvironment`)，然后把配置环境(`Environment`)加入监听对象中
4. 加载应用上下文(`ConfigurableApplicationContext`)，当做run方法的返回对象
5. 最后创建Spring容器，`refreshContext(context)`，实现starter自动化配置和bean的实例化等工作。
# SpringBoot自动配置(自动装配)、SpringFactoriesLoader
`SPI(Service Provider Interface)`：一种服务提供发现机制。SPI 的本质是将接口实现类的全限定名配置在文件中，并由服务加载器读取配置文件，加载实现类，在运行时给程序提供了良好的拓展功能。java中有ServiceLoader，Spring中有SpringFactoriesLoader，Dubbo中有ExtensionLoader。

SpringFactoriesLoader作为spring framework在3.2版本引入的SPI机制。

**SpringFactoriesLoader把META-INF/spring.factories作为properties文件解析**：
```java
Properties properties = PropertiesLoaderUtils.loadProperties(resource);
```
## SpringBoot自动配置流程
1. SpringFactoriesLoader会自动扫描并加载类路径下的所有spring.factories文件(所有包)，过滤出所有以@EnableAutoConfiguration类路径为属性key的url并返回。
2. Spring拿到要自动加载的类的全路径之后，会使用ConfigurationClassParser根据条件注解判断@Conditional等尝试解析成springBean到Spring容器。
## SpringFactoriesLoader
### 加载注解对应的配置类(不要求加载的类和对应的注解存在继承关系)
#### SpringFactoriesLoader会自动扫描并加载类路径下的所有spring.factories文件，过滤出所有以@EnableAutoConfiguration类路径为属性key的url并返回。

@EnableAutoConfiguration作为spring boot中自动装配的核心注解，在spring.factories中配置格式如下:
```factories
org.springframework.boot.autoconfigure.EnableAutoConfiguration=\
org.springframework.boot.actuate.autoconfigure.amqp.RabbitHealthContributorAutoConfiguration,\
org.springframework.boot.actuate.autoconfigure.audit.AuditAutoConfiguration,\
org.springframework.boot.actuate.autoconfigure.audit.AuditEventsEndpointAutoConfiguration,\
org.springframework.boot.actuate.autoconfigure.availability.AvailabilityHealthContributorAutoConfiguration,\
org.springframework.boot.actuate.autoconfigure.availability.AvailabilityProbesAutoConfiguration,\
org.springframework.boot.actuate.autoconfigure.beans.BeansEndpointAutoConfiguration,\
org.springframework.boot.actuate.autoconfigure.cache.CachesEndpointAutoConfiguration
```
调用方式：
```java
List<String> autoConfiguration = SpringFactoriesLoader.loadFactoryNames(EnableAutoConfiguration.class,Thread.currentThread().getContextClassLoader());
```
返回的autoConfiguration为：
```factories
org.springframework.boot.actuate.autoconfigure.amqp.RabbitHealthContributorAutoConfiguration
org.springframework.boot.actuate.autoconfigure.audit.AuditAutoConfiguration
org.springframework.boot.actuate.autoconfigure.audit.AuditEventsEndpointAutoConfiguration
org.springframework.boot.actuate.autoconfigure.availability.AvailabilityHealthContributorAutoConfiguration
org.springframework.boot.actuate.autoconfigure.availability.AvailabilityProbesAutoConfiguration
org.springframework.boot.actuate.autoconfigure.beans.BeansEndpointAutoConfiguration
org.springframework.boot.actuate.autoconfigure.cache.CachesEndpointAutoConfiguration
```
#### Spring拿到要自动加载的类的全路径之后，会使用ConfigurationClassParser根据条件注解判断@Conditional @ConditionalOnBean等尝试解析成springBean
### 加载接口对应的配置类
spring.factories里面还有其他配置，在`spring-boot-autoconfigure`、`org.springframework.boot`等`META-INF/spring.factories`下还有很多配置，如：
```factories
# Logging Systems
org.springframework.boot.logging.LoggingSystemFactory=\
org.springframework.boot.logging.logback.LogbackLoggingSystem.Factory,\
org.springframework.boot.logging.log4j2.Log4J2LoggingSystem.Factory,\
org.springframework.boot.logging.java.JavaLoggingSystem.Factory
# PropertySource Loaders
org.springframework.boot.env.PropertySourceLoader=\
org.springframework.boot.env.PropertiesPropertySourceLoader,\
org.springframework.boot.env.YamlPropertySourceLoader
# ConfigData Location Resolvers
org.springframework.boot.context.config.ConfigDataLocationResolver=\
org.springframework.boot.context.config.ConfigTreeConfigDataLocationResolver,\
org.springframework.boot.context.config.StandardConfigDataLocationResolver
....还有很多
```
SpringFactoriesLoader内部会自动加载并实例化作为对应类的实例，方便后续调用。
# SpringBoot核心注解
`@SpringBootApplication`注解是Spring Boot的核心注解，是一个组合注解，包括三个注解
### @Configuration
被标注的类等于在Spring的XML配置文件中（`applicationContext.xml`）,装配所有bean事务，提供了一个Spring的上下文环境
### @EnableAutoConfiguration
SpringBoot根据应用所声明的依赖来对Spring框架进行自动配置，其中包括两个重要注解：
- `@AutoConfigurationPackage`：该注解上有一个`@Import({Registrar.class})`注解，其中`Registrar`类的作用是将启动类所在的包下的所有子包组件扫描注入到spring容器中。**将controller、service等包放在启动类的同级目录下的原因**
- `@Import({AutoConfigurationImportSelector.class})`：
    - `AutoConfigurationImportSelector.getCandidateConfigurations()`这个方法通过`SpringFactoriesLoader.loadFactoryNames()`查找位于`META-INF/spring.factories`文件中的所有自动配置类并加载这些类。
    - `@Import({XxxxAutoConfiguration.class})`如此可直接加载SpringBoot扫描不到的配置类
### @ComponentScan
组件扫描，自动扫描和装配Bean，扫描`SpringApplication`的run方法中的`ExammanagerApplication.class`所在的包路径下的文件，因此将启动类（main）放在跟包路径下。它去找带有`@Component`注解的类，并为其创建bean。**@Controller、@Service等也是被@Component注解修饰的**
# 启动流程(详细)
Springboot的启动，主要创建了配置**环境(environment)、事件监听(listeners)、应用上下文(applicationContext)**，并基于以上条件，在容器中开始实例化我们需要的Bean
#### 1. 首先进入run()方法，run方法中new创建了一个SpringApplication实例
#### 2. 在SpringApplication的构造方法里为SpringApplication对象赋一些初值。
#### 3. 构造方法执行完后，回到run()方法。该方法中几个关键步骤：
1. 创建了应用的监听器SpringApplicationRunListeners并开始监听
2. 加载SpringBoot配置环境(ConfigurableEnvironment)，如果是通过web容器发布，会加载StandardEnvironment，其最终也是继承了ConfigurableEnvironment
3. 配置环境(Environment)加入到监听器对象中(SpringApplicationRunListeners)
4. 创建run方法的返回对象：ConfigurableApplicationContext(应用配置上下文)
5. 回到run方法内，prepareContext方法将listeners、environment、applicationArguments、banner等重要组件与上下文对象关联
6. 接下来的refreshContext(context)方法(初始化方法如下)将是实现spring-boot-starter-*(mybatis、redis等)自动化配置的关键，包括spring.factories的加载，bean的实例化等核心工作。
```java
public ConfigurableApplicationContext run(String... args) {
    long startTime = System.nanoTime();
    DefaultBootstrapContext bootstrapContext = this.createBootstrapContext();
    ConfigurableApplicationContext context = null;
    this.configureHeadlessProperty();
    //1.创建了应用的监听器SpringApplicationRunListeners并开始监听
    SpringApplicationRunListeners listeners = this.getRunListeners(args);
    listeners.starting(bootstrapContext, this.mainApplicationClass);
    try {
        ApplicationArguments applicationArguments = new DefaultApplicationArguments(args);
        //2.加载SpringBoot配置环境(ConfigurableEnvironment)，如果是通过web容器发布，会加载
        ConfigurableEnvironment environment = this.prepareEnvironment(listeners, bootstrapContext, applicationArguments);
        this.configureIgnoreBeanInfo(environment);
        //3.配置环境(Environment)加入到监听器对象中(SpringApplicationRunListeners)
        Banner printedBanner = this.printBanner(environment);
        //4.创建run方法的返回对象：ConfigurableApplicationContext(应用配置上下文)
        context = this.createApplicationContext();
        context.setApplicationStartup(this.applicationStartup);
        //5.回到run方法内，prepareContext方法将listeners、environment、applicationArguments、banner等重要组件与上下文对象关联
        this.prepareContext(bootstrapContext, context, environment, listeners, applicationArguments, printedBanner);
        //接下来的refreshContext(context)方法(初始化方法如下)将是实现spring-boot-starter-*(mybatis、redis等)自动化配置的关键，包括spring.factories的加载，bean的实例化等核心工作。
        this.refreshContext(context);
        this.afterRefresh(context, applicationArguments);
        Duration timeTakenToStartup = Duration.ofNanos(System.nanoTime() - startTime);
        if (this.logStartupInfo) {
            (new StartupInfoLogger(this.mainApplicationClass)).logStarted(this.getApplicationLog(), timeTakenToStartup);
        }
        listeners.started(context, timeTakenToStartup);
        this.callRunners(context, applicationArguments);
    } catch (Throwable var12) {
        this.handleRunFailure(context, var12, listeners);
        throw new IllegalStateException(var12);
    }
    try {
        Duration timeTakenToReady = Duration.ofNanos(System.nanoTime() - startTime);
        listeners.ready(context, timeTakenToReady);
        return context;
    } catch (Throwable var11) {
        this.handleRunFailure(context, var11, (SpringApplicationRunListeners)null);
        throw new IllegalStateException(var11);
    }
}
```
