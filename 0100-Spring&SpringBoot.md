# 面向面试

## ※Spring IOC 概述

IOC容器就是存放和管理SpringBean的容器。`BeanFactory`是IOC容器实现的顶级接口，一般是Spring框架内部使用，开发者用到比较多的是它的一个子接口`ApplicationContext`，这也是常说的应用上下文，常见的**Bean工厂功能**、**事件传播**、**资源加载**、**AOP支持**、**Bean生命周期管理**等功能都有支持。

## ApplicationContext加载定义配置的方式

Spring自带了多种类型的应用上下文实现，不同应用上下文的区别就在于加载配置的方式不同，如下几种是常使用的加载方式：

- AnnotationConfigApplicationContext：从一个或多个基于Java的配置类中加载应用上下文（在前面注解驱动中有使用到）；
- ClassPathXmlApplicationContext：从类路径下的一个户多个XML配置文件中加载上下文定义，把应用上下文的定义文件作为类资源（最常用的一种方式）；
- FileSystemXmlApplicationContext：从文件系统下的一个或多个XML配置文件中加载上下文定义。

另外还有WebApplicationContext接口继承了ApplicationContext，它是专门为Web应用而准备的，它允许从相对于Web根目录的路径中完成初始化工作。

## ※※※Spring IOC 创建过程

IOC容器的创建过程在`public abstract class AbstractApplicationContext extends DefaultResourceLoader  
      implements ConfigurableApplicationContext`中实现，它实现了`ApplicationContext`接口。其创建过程如下：

### 极简

1. 刷新前的预处理`prepareRefresh()`。

2. 获取新的BeanFactory`ConfigurableListableBeanFactory beanFactory = obtainFreshBeanFactory()`。

3. 准备BeanFactory，为其做一些设置`prepareBeanFactory(beanFactory)`。

4. BeanFactory后置处理工作，Spring未作实现`ostProcessBeanFactory(beanFactory)`。

5. 创建并执行BeanFactoryPostProcessor`invokeBeanFactoryPostProcessors(beanFactory)`。

6. 创建并注册BeanPostProcessor`registerBeanPostProcessors(beanFactory)`。

7. 国际化组件`initMessageSource()`。

8. 初始化事件广播器`initApplicationEventMulticaster()`。

9. onRefresh()空方法。

10. 注册监听器`registerListeners()`。

11. 实例化所有剩余的(非懒加载)单例Bean`finishBeanFactoryInitialization(beanFactory)`。

12. 完成刷新后发布相应事件`finishRefresh()`。

### 丰富

###### 1.刷新前的预处理`prepareRefresh()`。

设置容器启动时间、状态，初始化属性资源，获取并验证环境信息。

###### 2.获取新的BeanFactory`ConfigurableListableBeanFactory beanFactory = obtainFreshBeanFactory()`。

获取一个新的BeanFactory，用于存储和管理bean对象以及bean元数据信息，如果是第一次创建容器，就创建一个DefaultListableBeanFactory类型的bean工厂，如果是刷新容器，就销毁旧的bean工厂，重新创建一个新的bean工厂。加载`BeanDefinition`注册到`BeanDefinitionRegistry`。Bean元数据([BeanDefinition](#BeanDefinition))就包含了**Bean类名、名称、别名、描述、作用域、是否懒加载、自动装配模式、依赖、初始化方法、销毁方法、构造函数参数、工厂方法等信息。**

###### 3.准备BeanFactory，为其做一些设置`prepareBeanFactory(beanFactory)`。

设置类加载器、SpEL解析器、一些环境相关的Bean、添加一个`ApplicationContextAwareProcessor`类型的的BeanPostProcessor，用于创建Bean时部分Aware接口的回调。

###### 4.BeanFactory后置处理工作，Spring未作实现`ostProcessBeanFactory(beanFactory)`。

BeanFactory准备工作完成后进行的后置处理工作，这是一个空的protected方法，子类可以通过重写这个方法来在BeanFactory创建并准备完成以后做进一步的设置。

###### 5.创建并执行BeanFactoryPostProcessor`invokeBeanFactoryPostProcessors(beanFactory)`。

创建并执行BeanFactoryPostProcessor。我们可以实现`BeanFactoryPostProcessor`、`BeanDefinitionRegistryPostProcessor(前者子接口)`等接口操作BeanFactory，比如获取Bean元数据、自定义注册Bean元数据。如果此处有很多PostProcessor，会根据子先父后、Ordered接口指定的顺序执行。

###### 6.创建并注册BeanPostProcessor`registerBeanPostProcessors(beanFactory)`。

按先父后、Order顺序创建并且注册BeanPostProcessor，注册MergedBeanDefinitionPostProcessor，注册一个ApplicationListenerDetector类型的后置处理器(此处理器会注册一些我们自定义的监听器)

###### 7.国际化组件`initMessageSource()`。

初始化MessageSource组件，该组件可以取出国际化配置文件中某个key的值，能按照区域信息获取。

###### 8.初始化事件广播器`initApplicationEventMulticaster()`。

初始化`ApplicationEventMulticaster`类型的事件派发器

###### 9.onRefresh()空方法。

这是一个空的protected方法，便于子类拓展，可以在这里创建一些特殊的Bean，Spring Boot即是在这一步创建的内嵌的Servlet容器并启动。

###### 10。注册监听器`registerListeners()`。

注册一些已经实例化的监听器(`getApplicationListeners()`)，发布一些早期事件`earlyEventsToProcess`(一般为空)

###### 11.实例化所有剩余的(非懒加载)单例Bean`finishBeanFactoryInitialization(beanFactory)`。

[创建剩下的Bean。](#finishBeanFactoryInitialization)

###### 12.完成刷新后发布相应事件`finishRefresh()`。

清除资源缓存、初始化BeanFactory生命周期处理器、将刷新完毕事件传播到生命周期处理器、推送上下文刷新完毕事件到相应的监听器

### 完整

[IOC创建过程 by陈冉 有道云笔记 (youdao.com)](https://note.youdao.com/ynoteshare/index.html?id=67e3693862a1a06843f073e66faa2cf8&type=notebook&_time=1709600614341#/WEB6500f38197f9e8dfbddab183f766280a)

## ※※※SpringBean创建过程

<a id="finishBeanFactoryInitialization"></a>

这里对应IOC创建的`finishBeanFactoryInitialization(beanFactory)`。创建非抽象、非懒加载、是单例的Bean。

### 极简

1. 冻结配置，不再允许修改工厂的配置信息。

2. 在三个缓存查找bean，找不到就创建。

3. 根据`BeanDefinition`使用反射创建对象。

4. 调用初始化前的后置处理器。

5. 调用初始化方法。

6. 调用初始化后的后置处理器。

![](assets/2024-03-05-13-18-02-image.png)

### 丰富

###### 冻结配置

容器无法在配置和容器内部对象一直变更的情况下对所有单实例bean进行初始化，所以在实例化之前需要先冻结bean工厂的配置操作，不再允许修改工厂的配置信息。

###### 创建Bean对象`preInstantiateSingletons()`

- 拿到`beanDefinitionNames`循环根据`BeanDefinition`创建对象(只是作为参数往下传)。

- 如果是工厂Bean`FactoryBean`则先创建工厂本身，再用此工厂创建Bean。

- 不是`FactoryBean`直接`getBean(beanName)`。

##### doGetBean()

- 其中`getSingleton(beanName)`依次从`singletonObjects、earlySingletonObjects、singletonFactories`三个缓存中获取bean对象。如果是在`singletonFactories`中获取到，则直接会用工厂创建出早期对象put进`earlySingletonObjects`，最后在`singletonFactories`中删除此beanName。
  
  - 这里用`synchronized`锁了`singletonObjects`并且做了双重检查。

- 如果三个缓存都未找到bean，则根据`BeanDefinition`创建对象(只是作为参数往下传)。

##### createBean()

在`resolveBeforeInstantiation(beanName, mbdToUse)`方法中调用容器中所有`InstantiationAwareBeanPostProcessor`类型的后置处理器的初始化前(`postProcessBeforeInstantiation(beanClass, beanName)`)方法，如果 `BeanPostProcessor` 在 `resolveBeforeInstantiation` 方法中返回了一个非 `null` 的对象，那么这个对象就会作为 bean 的实例返回，而不是继续执行后续的 bean 创建过程。这通常发生在 AOP（面向切面编程）代理的创建过程中，其中 `BeanPostProcessor` 可以检测 bean 是否需要被代理，并相应地创建一个代理对象。

###### resolveBeforeInstantiation()方法创建代理：

- 当您希望在实例化目标Bean之前就创建AOP代理对象时，通常会在`resolveBeforeInstantiation()`方法中进行。
- 这种情况通常出现在您希望在Bean实例化之前就应用切面的情况下。
- 典型情况包括：使用BeanFactoryPostProcessor或者BeanPostProcessor接口进行自定义处理时，或者通过编程方式配置AOP代理对象。

##### doCreateBean()

该方法实现于`AbstractAutowireCapableBeanFactory`

###### 实例化bean`createBeanInstance()`

调用了instantiateBean()方法，这里面又依次调用了instantiate()，BeanUtils.instantiateClass()，在最后一个方法中通过反射的方式创建了对象实例。

###### MergedBeanDefinitionPostProcessor的后置处理

调用所有的`MergedBeanDefinitionPostProcessor`类型的后置处理器的`postProcessMergedBeanDefinition()`方法（其中包括 AutowiredAnnotationBeanPostProcessor对象）

- `MergedBeanDefinitionPostProcessor`的主要职责是在bean实例化后，合并bean定义的后置处理器。这包括将注解（如`@Autowired`）转换成相应的元素（如`InjectedElement`），以便在后续的步骤中设置值。此外，`MergedBeanDefinitionPostProcessor`还负责快速获取bean的所有指定信息以及清理缓存中bean的指定信息。在bean的生命周期中，这个方法通常在创建实例化对象后，赋值方法前执行。

- `AutowiredAnnotationBeanPostProcessor`则是对注解`@Autowired`的实现。这个处理器会自动将bean的依赖项注入到bean中。例如，如果一个bean依赖于另一个bean B，那么`AutowiredAnnotationBeanPostProcessor`会负责将B注入到依赖它的bean中。这个过程是在`populateBean()`方法中进行的，这是bean初始化过程的一部分。

- `InitDestroyAnnotationBeanPostProcessor`处理带有PostConstruct注解方法的执行等等

###### 属性赋值`populateBean()`

- 拿到所有的`InstantiationAwareBeanPostProcessor`，调用其`postProcessAfterInstantiation`方法；
- 拿到所有的`InstantiationAwareBeanPostProcessor`，调用其`postProcessPropertyValues`方法；
- 应用Bean属性的值，利用setter方法为属性赋值
- CommonAnnotationBeanPostProcessor继承自InitDestroyAnnotationBeanPostProcessor并作为InstantiationAwareBeanPostProcessor和MergedBeanDefinitionPostProcessor的接口实现完成对 @Resource 等注解属性的注入操作。

###### 执行初始化方法`initializeBean()`

- 调用容器中`Aware`的接口的实现类的setter方法，不过这里只有一些beanFactory相关的接口， `ApplicationContext`相关的`Aware`接口则是在`ApplicationContextAwarePostProcessor`的前置处理调用的。
  
  - AOP 相关的 `BeanPostProcessor` 可能会创建 AOP 代理对象，并将其应用到目标 Bean 上。
  
  - - 当您希望在目标Bean实例化后，但在对其初始化之前创建AOP代理对象时，通常会在`doCreateBean()->initializeBean()`方法中进行。
    - 这种情况通常出现在您希望在Bean实例化后再应用切面的情况下。
    - 典型情况包括：当您使用基于注解的AOP配置，并且希望Spring在实例化目标Bean后应用切面时。

- 回调`BeanPostProcessor`的`postProcessBeforeInitialization`方法。它会遍历所有注册的 `BeanPostProcessor`，然后在每个 `BeanPostProcessor` 中调用 `postProcessBeforeInitialization` 方法。

- 调用Bean的init方法。init相关方法主要指两个，第一个是实现了`InitializingBean`接口后实现的`afterPropertiesSet()`方法，第二个是自定义的init方法。

- 回调`BeanPostProcessor`的`postProcessAfterInitialization`方法。它会遍历所有注册的 `BeanPostProcessor`，然后在每个 `BeanPostProcessor` 中调用 `postProcessAfterInitialization` 方法。

###### 注册DisposableBean`registerDisposableBeanIfNecessary()`

在Bean实例化和初始化完成后，容器可能会注册一些DisposableBean，以确保在容器关闭时能够正确地销毁这些Bean。

### 完整

 [Bean的创建流程 by陈冉 有道云笔记 (youdao.com)](https://note.youdao.com/ynoteshare/index.html?id=67e3693862a1a06843f073e66faa2cf8&type=notebook&_time=1709600614341#/WEBd0c3ca9f9d498724dbcce6233fe3a641)

 [Bean的创建流程 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/458001502)

# 其他知识点

##### BeanDefinition中的元数据信息

<a id="BeanDefinition"></a>

1. **Bean的类名（Class Name）**：描述了Bean所属的类。

2. **Bean的ID**：唯一标识符，用于在Spring容器中识别Bean。

3. **Bean的作用域（Scope）**：描述了Bean的生命周期范围，如singleton、prototype等。

4. **Bean的构造函数参数**：描述了Bean实例化时所需要的构造函数参数，包括参数类型、参数值等。

5. **Bean的属性（Properties）**：描述了Bean的各种属性值，可以是基本类型、引用类型或集合类型。

6. **Bean的初始化方法**：描述了Bean实例化后需要执行的初始化方法。

7. **Bean的销毁方法**：描述了Bean在容器关闭时需要执行的销毁方法。

8. **Bean的依赖关系**：描述了Bean与其他Bean之间的依赖关系，如依赖注入。

9. **Bean的自动装配模式**：描述了Spring容器如何自动装配Bean之间的依赖关系。

10. **Bean的描述信息（Description）**：提供了对Bean的简要描述，用于在容器中进行标识和管理。

##### 动态代理过程

[Spring AOP by陈冉 有道云笔记 (youdao.com)](https://note.youdao.com/ynoteshare/index.html?id=67e3693862a1a06843f073e66faa2cf8&type=notebook&_time=1709600614341#/WEBb1cbb36239847a76913059d357e2cff3)

[代理模式 by陈冉 有道云笔记 (youdao.com)](https://note.youdao.com/ynoteshare/index.html?id=67e3693862a1a06843f073e66faa2cf8&type=notebook&_time=1709600614341#/WEB4ec6e6f1e1cc460cdf91d2721cb6072a)

[Spring AOP创建Proxy的过程 - 知乎 (zhihu.com)](https://www.zhihu.com/question/591322463/answer/2951163718?utm_id=0)

JDK动态代理创建代理对象的过程如下：

1. 定义切面类和切面方法。
2. Spring框架使用反射机制动态创建一个代理类，该代理类实现目标对象的所有接口，同时继承Proxy类。
3. 在代理类中，每个切入点都被表示为一个方法，该方法调用目标对象的对应方法，并在方法调用前后调用切面方法。
4. 将代理类的实例化过程封装到ProxyFactory中，并且设置好代理类的目标对象、切面类和代理方式等信息。
5. 通过ProxyFactory创建代理对象。

CGLIB动态代理创建代理对象的过程如下：

1. 定义切面类和切面方法。
2. Spring框架使用ASM字节码操作库在运行时动态生成一个目标类的子类，该子类重写了目标对象的所有非final方法。
3. 在子类中，每个切入点都被表示为一个方法，该方法调用目标对象的对应方法，并在方法调用前后调用切面方法。
4. 将子类的实例化过程封装到Enhancer中，并且设置好子类的目标对象、切面类和代理方式等信息。
5. 通过Enhancer创建代理对象。

无论是使用JDK动态代理还是CGLIB动态代理，Spring AOP的创建代理对象的过程都是动态生成一个新的代理类或者目标类的子类，并在这个新类中织入切面逻辑，从而实现对目标对象的增强。

# SpringBoot

[by陈冉 有道云笔记 (youdao.com)](https://note.youdao.com/ynoteshare/index.html?id=67e3693862a1a06843f073e66faa2cf8&type=notebook&_time=1709600614341#/WEB085273001a77e992267074b8a2296e20)

## ※概述

虽然Spring的组件代码是轻量级的，但是它的配置却是重量级的，需要很多XML配置；Spring2.5引入了基于注解的组件扫面，这消除了大量针对应用程序自身组件的显式XML配置；

Spring3.0引入了基于Java的配置，这是一种安全的可重构的配置方式，可以代替XML。但是开启Spring的某些特性时，比如事务管理和SpringMVC，还是需要用XML或Java显式配置；启用第三方库和配置Servlet和过滤器等同样需要配置。除此之外，项目的依赖管理也是一件棘手的事情。

**Spring Boot用来简化Spring应用开发，约定大于配置，其核心功能是：自动配置、起步依赖、命令行界面、Actuator**

## SpringBoot容器初始化过程

run方法代码：

```java
public ConfigurableApplicationContext run(String... args) {
    // 创建StopWatch，用于统计Springboot启动的耗时
    StopWatch stopWatch = new StopWatch();
    // 开始计时
    stopWatch.start();
    DefaultBootstrapContext bootstrapContext = createBootstrapContext();
    ConfigurableApplicationContext context = null;
    configureHeadlessProperty();
    // 获取运行时监听器
    SpringApplicationRunListeners listeners = getRunListeners(args);
    // 调用运行时监听器的starting()方法
    // 该方法需要在Springboot一启动时就调用，用于特别早期的初始化
    listeners.starting(bootstrapContext, this.mainApplicationClass);
    try {
        // 获取args参数对象
        ApplicationArguments applicationArguments = new DefaultApplicationArguments(args);
        // 读取Springboot配置文件并创建Environment对象
        // 这里创建的Environment对象实际为ConfigurableEnvironment
        ConfigurableEnvironment environment = prepareEnvironment(listeners, bootstrapContext, applicationArguments);
        configureIgnoreBeanInfo(environment);
        // 打印Banner图标
        Banner printedBanner = printBanner(environment);
        // 创建ApplicationContext应用行下文，即创建容器
        context = createApplicationContext();
        context.setApplicationStartup(this.applicationStartup);
        // 准备容器
        prepareContext(bootstrapContext, context, environment, listeners, applicationArguments, printedBanner);
        // 初始化容器----这里就是spring IOC初始化
        refreshContext(context);
        afterRefresh(context, applicationArguments);
        // 停止计时
        stopWatch.stop();
        if (this.logStartupInfo) {
            // 打印启动耗时等信息
            new StartupInfoLogger(this.mainApplicationClass).logStarted(getApplicationLog(), stopWatch);
        }
        // 调用运行时监听器的started()方法
        // 该方法需要在应用程序启动后，CommandLineRunners和ApplicationRunners被调用前执行
        listeners.started(context);
        callRunners(context, applicationArguments);
    }
    catch (Throwable ex) {
        handleRunFailure(context, ex, listeners);
        throw new IllegalStateException(ex);
    }

    try {
        // 调用运行时监听器的running()方法
        // 该方法需要在SpringApplication的run()方法执行完之前被调用
        listeners.running(context);
    }
    catch (Throwable ex) {
        handleRunFailure(context, ex, null);
        throw new IllegalStateException(ex);
    }
    return context;
}
```

- prepareRefresh()：准备刷新容器，记录容器的启动时间，设置容器的活动状态，初始化属性源和事件集合等。
- obtainFreshBeanFactory()：获取一个新的bean工厂，用于存储和管理bean对象，如果是第一次创建容器，就创建一个DefaultListableBeanFactory类型的bean工厂，如果是刷新容器，就销毁旧的bean工厂，重新创建一个新的bean工厂。
- prepareBeanFactory(beanFactory)：对获取到的bean工厂进行预处理，设置类加载器，表达式语言解析器，属性编辑器，忽略的自动装配接口，注册一些系统内置的bean和后置处理器等。
- postProcessBeanFactory(beanFactory)：对bean工厂进行后置处理，这是一个空方法，留给子类覆盖，用于在bean定义加载完成后，bean实例化之前做一些额外的处理。
- invokeBeanFactoryPostProcessors(beanFactory)：执行bean工厂的后置处理器，这些后置处理器可以对bean定义进行修改或添加，分为两类：BeanDefinitionRegistryPostProcessor和BeanFactoryPostProcessor，前者可以在bean定义注册阶段执行，后者可以在bean定义加载阶段执行，springboot会在这一步骤中执行自动配置类，根据条件注解和类路径依赖来动态注册bean定义。
- registerBeanPostProcessors(beanFactory)：注册bean的后置处理器，这些后置处理器可以对bean实例进行修改或添加，分为多种类型，例如InstantiationAwareBeanPostProcessor，DestructionAwareBeanPostProcessor，BeanPostProcessor等，它们可以在bean的生命周期的不同阶段执行，例如实例化前后，初始化前后，销毁前后等。
- initMessageSource()：初始化MessageSource组件，用于国际化消息的处理，可以根据不同的区域设置来解析消息，springboot会在这一步骤中注册一个默认的ReloadableResourceBundleMessageSource，用于加载类路径下的messages.properties文件。
- initApplicationEventMulticaster()：初始化事件广播器，用于管理事件的发布和监听，springboot会在这一步骤中注册一个默认的SimpleApplicationEventMulticaster，用于同步或异步地广播事件给注册的监听器。
- onRefresh()：留给子类覆盖的方法，用于在所有的单例bean初始化之前做一些额外的处理，**boot在web容器中注册一些servlet，filter，listener等**。
- registerListeners()：注册事件监听器，用于接收和处理事件，**springboot会在这一步骤中注册一些内置的监听器，例如LoggingApplicationListener，ConfigFileApplicationListener，DelegatingApplicationListener等**，以及用户自定义的监听器，这些监听器可以在容器启动或关闭的过程中触发一些事件，例如ApplicationStartingEvent，ApplicationEnvironmentPreparedEvent，ApplicationReadyEvent等。
- finishBeanFactoryInitialization(beanFactory)：完成bean工厂的初始化，用于实例化所有的非懒加载的单例bean，包括以下步骤：创建bean实例，注入依赖，调用初始化方法，注册销毁方法，应用后置处理器等，这是bean生命周期的核心阶段，**springboot会在这一步骤中创建并初始化一些重要的组件，例如Environment，PropertySources，ConfigurationProperties，CommandLineRunner等**。
- finishRefresh()：完成容器的刷新，用于通知生命周期处理器和事件监听器容器刷新完成，以及清理一些临时状态，**springboot会在这一步骤中执行一些启动器，例如调用CommandLineRunner的run方法，发布ApplicationReadyEvent事件等**。

## ※※※SpringBoot自动配置

[自动配置&起步依赖 by陈冉 有道云笔记 (youdao.com)](https://note.youdao.com/ynoteshare/index.html?id=67e3693862a1a06843f073e66faa2cf8&type=notebook&_time=1709600614341#/WEBcd0ba52ec1b6f6ef857a9ebe1353d08e)

### @Import注解

- `@Import({A.class,B.class})`，这样容器会自动注册a,b组件。

- `@Import(ImportSelector实现类.class)`配合自定义的`ImportSelector`实现导入的组件的全类名数组，一次导入多个。

- `@Import(ImportBeanDefinitionRegistrar实现类.class)`配合自定义的`ImportBeanDefinitionRegistrar`实现注册多个`BeanDefinition`。

### 自动配置原理

1. 启动类标记了`@SpringBootApplication`注解。它里是一个符合注解，它又标记了`@EnableAutoConfiguration`注解。

2. `@EnableAutoConfiguration`注解又被标记了`@AutoConfigurationPackage`和`@Import(AutoConfigurationImportSelector.class)`注解。

3. `@AutoConfigurationPackage`注解上有`@Import(AutoConfigurationPackages.Registrar.class)`，它注册了启动类同级包及以下所有的组件。

4. `@Import(AutoConfigurationImportSelector.class)`，此选择器基于SPI机制，会加载`AutoConfiguration`类的候选者。这些候选者通常是一组预定义的自动配置类，它们存放在Spring Boot的jar包中的`META-INF/spring.factories`文件中。

5. 拿到所有候选者的全类名数组后，删除掉我们指定排除的，再根据`Configuration`过滤掉不满足条件的，最后把剩下的注册到`BeanDefinition`。

## ※※※SpringBoot起步依赖 (Starter)

起步依赖就是`starter`，它需要用到自动配置功能。

起步依赖就是特殊依赖的Maven依赖，利用了依赖的传递性，把常用库聚合在一起，组成了几个为特定功能而定制的依赖。

### Starter

在Spring Boot中，将starter分为starter和configuration两个包的主要原因是为了清晰地分离依赖管理和自动配置的功能。这种做法有助于提高代码的可维护性、可读性和模块化。

1. **依赖管理（Starter）**：
   
   Starter是一个Maven或Gradle的依赖集合，它包含了构建某个特定类型的应用所需的所有基本依赖。例如，`spring-boot-starter-web`包含了构建Web应用所需的所有核心依赖，如Spring MVC、Tomcat等。这些依赖是静态的，它们只是简单地告诉构建工具需要下载和包含哪些库。
   
   将依赖放在单独的starter包中，可以确保开发者只需添加一个依赖，就能获得所需功能所需的所有库，而无需手动管理这些库的版本和依赖关系。

2. **自动配置（Configuration）**：
   
   自动配置是Spring Boot的核心特性之一，它根据类路径、属性设置和其他条件自动配置应用。这通常是通过在`META-INF/spring.factories`文件中注册一系列的配置类来实现的。这些配置类包含了创建和配置bean的逻辑。
   
   将自动配置逻辑放在单独的configuration包中，有几个好处：
   
   - **模块化**：自动配置逻辑与依赖管理逻辑分离，使得每个部分都更加模块化，更易于维护和测试。
   - **可读性**：开发者可以更容易地理解哪些代码负责依赖管理，哪些代码负责自动配置。
   - **灵活性**：通过将自动配置逻辑放在单独的包中，开发者可以更容易地覆盖或禁用特定的自动配置，而不影响依赖管理。
   - **可重用性**：某些自动配置逻辑可能需要在多个starter之间共享。通过将它们放在单独的包中，可以更容易地实现这种共享。

### 自定义Starter

- 需要创建`XXX-spring-boot-starter和XXX-spring-boot-configuration`两个模块。`starter`模块用于依赖配置，引入必须使用的依赖，如`XXX-spring-boot-configuration`等。`configuration`模块用于自动配置。

- `configuration`模块需要引入`spring-boot-starter-parent、spring-boot-autoconfigure`等必要依赖，其他用于编译的依赖可以标记`<optional>true</optional>`。

- `configuration`模块一般需要三块内容，自定义的`Properties、AutoConfiguration`类，以及功能所需要注册为Bean的类。

- 在自动配置类(`AutoConfiguration`)中我们需要标记其为配置类`@Configuration`。

- 需要用`@EnableConfigurationProperties`装载配置类而不是直接在配置类标记`@Component`。

- 需要用到`@Conditional`下的各子注解标记此自动配置加载的条件。

- 需要`@Bean`等方式注册我们的Bean，并用`@Conditional`配置注册条件。

- 需要将我们的`utoConfiguration`类全类名配置到`META-INF/spring.factories`。

- SpringBoot3.X后弃用`META-INF/spring.factories`，采用`META-INF/spring/org.springframework.boot.autoconfigure.AutoConfiguration.imports`。
  
  ```
  文件路径都变了，也就是3.X的文件里不需要再写
  org.springframework.boot.autoconfigure.EnableAutoConfiguration=\
  com.xxx.xxx.XxxxAutoConfiguration
  可以直接写
  com.xxx.xxx.XxxxAutoConfiguration
  ```

 

如果自动配置类和资源很少或者用到的自动配置类和资源是由 Spring Boot 官方提供的也可以只要`starter`模块，将`configuration`模块的代码集成进去。

# SpringMVC

# 常见面试问题

##### 循环依赖Spring无法自动解决

以下常见都举例为A、B对象互相依赖且Spring先创建A对象。

- SpringBoot2.7+版本后不再默认解决循环依赖，需要添加一个配置allowRawInjectionDespiteWrapping=true，但是这样B拿到的A的实例是原始对象，而不是初始化过后的，就没有各种BeanPostProcessor的增强功能。

- 采用构造器依赖注入方式。spring解决循环依赖实在初始化阶段，如果在构造方法中注入依赖，就会导致在实例化bean的阶段遇到循环依赖。**A对象字段注入，B对象构造器注入是可以解决循环依赖的。**

- 原型Bean(prototype作用域)。每次获取对象都会创建新对象，不会用到缓存，就会死循环创建。**A对象单例，B对象为原型，这样是可以解决循环依赖的。**

- @DependsOn注解的对象。此注解用于制定当前bean依赖于其他bean，Spring创建当前bean前会先去创建被指定的bean。

- @Async注解A对象的方法。出现循环依赖时，使用 @Async 注解会抛出异常。
  
  - 在对象A 的初始化后阶段，会遍历所有的后置处理器，并执行它们的初始化后的方法。其中，我们需要提到的第一个后置处理器是 `AnnotationAwareAspectAutoProxyCreator`，它主要用于处理 AOP。如果存在循环依赖，该后置处理器会直接返回对象 A的原始对象，不管对象A是否需要进行 AOP。假设后续没有其他后置处理器对这个原始对象进行修改了，对象 A 就会去二级缓存中取出半成品对象并作为最终的返回对象。此时，我们可以理解为对象A和对象B属性填充阶段从三级缓存中执行 Lambda 后注入的对象 A 是同一个对象
  
  - 如果为使用了 @Async 注解， Spring 在对象 A 的初始化后阶段，还会执行一个后置处理器(`AsyncAnnotationBeanPostProcessor`) ，该后置处理器就会修改对象 A 的返回结果为另外一个代理对象，此时，对象B注入的对象 A和这个修改后的对象 A就不是同一个对象了

如果是B对象使用@Async则没问题。

使用@Lazy标记A对象中的B属性可以解决此问题，或者使用@DependsOn注解让使用了@Aync注解的对象先后加载。

##### Bean的创建顺序，一堆Bean，先创建谁后创建谁

在 Spring 中，Bean 的创建顺序由它们的依赖关系和作用域决定。通常情况下，Spring 在创建 Bean 时会遵循以下规则：

1. **依赖关系：** 如果一个 Bean 依赖于另一个 Bean，Spring 会首先创建被依赖的 Bean，然后再创建依赖于它的 Bean。这意味着，如果 Bean A 依赖于 Bean B，则 Spring 会先创建 Bean B，然后再创建 Bean A。

2. **作用域：** 在同一作用域下，Spring 会按照 Bean 在配置文件中的顺序来创建它们。例如，在 XML 配置文件中，Bean 的定义顺序决定了它们的创建顺序。如果 Bean A 在 Bean B 的前面定义，则 Spring 会先创建 Bean A，然后再创建 Bean B。

3. **生命周期回调方法：** 如果 Bean 实现了 InitializingBean 接口或者定义了初始化方法（通过 init-method 属性），Spring 会在创建 Bean 后立即调用这些方法，因此在创建 Bean 时会考虑这些回调方法的执行顺序。

Spring 容器会根据扫描到的 Bean 类的顺序来创建 Bean。通常情况下，Classpath 下的类文件是按照字母和数字顺序进行扫描的。

在 Spring 中，不同的配置方式会导致不同的 Bean 扫描顺序。以下是对于各种配置方式 Spring 扫描顺序的说明：

1. **XML 配置：** 在使用 XML 配置时，Spring 首先会解析加载 XML 配置文件。在 XML 文件中定义的 Bean 会被 Spring 依次扫描并注册。因此，XML 配置文件中定义的 Bean 会被先扫描。

2. **@ComponentScan 配置：** 在使用注解配置时，通常会使用 `@ComponentScan` 注解来指定需要扫描的包路径。Spring 会从指定的包路径开始递归扫描，扫描到的带有 `@Component`、`@Service`、`@Repository` 注解的类会被注册为 Bean。因此，`@ComponentScan` 指定的包路径中的类会被先扫描。**用@ComponentScan配置了包路径，Spring就不会默认扫描@SpringBootApplication注解标记的类同目录及以下的@Component了**

3. **@Component、@Service、@Repository 注解：** 在使用注解配置时，使用了 `@Component`、`@Service`、`@Repository` 注解标注的类会被 Spring 扫描并注册为 Bean。因此，使用了这些注解的类会被先扫描。

4. **@Bean 注解：** 使用 `@Bean` 注解在配置类中定义的 Bean 会在扫描后被注册。因此，`@Bean` 注解定义的 Bean 会在上述注解扫描后被注册。

5. **@Import 注解：** 使用 `@Import` 注解导入的配置类会在扫描后被加载和解析。因此，被 `@Import` 导入的配置类中定义的 Bean 会在上述所有 Bean 扫描后被注册。

综上所述，当 Spring 启动时，首先会加载 XML 配置文件（如果有的话），然后扫描指定的包路径和注解配置，依次注册 Bean。因此，XML 配置文件中定义的 Bean、通过 `@ComponentScan` 扫描到的类、使用了 `@Component`、`@Service`、`@Repository` 注解的类会被先扫描和注册。然后，`@Bean` 注解定义的 Bean 和通过 `@Import` 导入的配置类中的 Bean 会在上述 Bean 扫描后被注册。

##### FactoryBean

工场Bean，假如我们实现了FactoryBean，会在实例化单体bean的方法中(`preInstantiateSingletons`)特殊处理，先创建工厂本身，再按照工厂创建Bean，最后创建好的Bean是FactoryBean所创建的Bean。

此接口是spring为我们提供自定义或者说个性化、定制化Bean创建过程的规范。

##### 三级缓存

三级缓存只是民间称呼，官方没有此称呼。

在DefaultSingletonBeanRegistry(默认单例Bean注册表)类中，有三个缓存：

```java
    /** Cache of singleton objects: bean name to bean instance. */
    //singleton对象的缓存
    //一级缓存，存放完全实例化且属性赋值完成的 Bean ，可以直接使用
    private final Map<String, Object> singletonObjects = new ConcurrentHashMap<>(256);

    /** Cache of singleton factories: bean name to ObjectFactory. */
    //单例工厂的缓存
    //三级缓存，存放实例化完成的 Bean 工厂
    private final Map<String, ObjectFactory<?>> singletonFactories = new HashMap<>(16);

    /** Cache of early singleton objects: bean name to bean instance. */
    //早期singleton对象的缓存
    //二级缓存，存放早期 Bean 的引用，尚未装配属性的 Bean
    private final Map<String, Object> earlySingletonObjects = new ConcurrentHashMap<>(16);
```

1. singletonObjects：这个缓存用于存储已经完全初始化的单例bean实例。当容器初始化时，如果某个bean是单例的，并且它的依赖关系已经解析完毕，并且它的初始化方法已经执行完毕，那么这个bean就会被放入singletonObjects缓存中。

2. singletonFactories：这个缓存用于存储尚未完全初始化的单例bean的工厂对象。当容器初始化时，如果某个bean是单例的，但是它的依赖关系尚未解析完毕，或者它的初始化方法尚未执行完毕，那么这个bean的工厂对象会被放入singletonFactories缓存中。

3. earlySingletonObjects：这个缓存用于存储尚未完全初始化的单例bean实例。当容器初始化时，如果某个bean是单例的，但是它的依赖关系尚未解析完毕，或者它的初始化方法尚未执行完毕，那么这个bean的早期实例会被放入earlySingletonObjects缓存中。这个缓存用于解决循环依赖问题。

除了循环依赖问题外，earlySingletonObjects缓存还可能在以下情况下使用：

1. 预先实例化：某些情况下，Spring可能会在容器启动时预先实例化某些单例bean，以加速应用程序的启动时间。在这种情况下，即使没有循环依赖，早期实例也会被放入earlySingletonObjects缓存中。

2. 提前曝光：除了解决循环依赖问题外，Spring可能还会在bean的生命周期中的早期阶段提前暴露bean实例给其他bean。这样可以确保其他bean能够访问到正在创建的bean实例，即使它的初始化方法尚未执行完成。这种情况下，早期实例也会被放入earlySingletonObjects缓存中

##### 循环依赖

![循环依赖](assets/CircularDependency.jpg)

##### 在SpringBoot的启动过程中，开发人员可以做哪些事情

**自定义启动逻辑：** 可以通过实现`ApplicationRunner`或`CommandLineRunner`接口来编写自定义的启动逻辑。这些接口允许开发人员在Spring Boot应用程序启动时执行特定的代码。
