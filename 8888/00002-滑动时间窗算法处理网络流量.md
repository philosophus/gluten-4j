##### 20231222 文心一言4.0

滑动时间窗算法在处理网络流量时，主要的逻辑如下：

1. 设定时间窗口大小和滑动间隔：首先，需要设定一个时间窗口的大小，比如一分钟，以及滑动间隔，比如每六秒滑动一次。时间窗口的大小和滑动间隔可以根据实际需求进行调整。
2. 初始化计数器：在时间窗口内，通常会设定多个计数器，每个计数器对应一个更小的时间间隔。比如，如果时间窗口是一分钟，可以设定10个计数器，每个计数器对应6秒的时间间隔。
3. 接收并处理请求：当网络请求到达时，会根据请求到达的时间点确定其所属的时间间隔，并相应地增加该时间间隔的计数器的计数。
4. 检查并触发限流操作：在每个滑动间隔结束时，会检查时间窗口内所有计数器的计数总和是否超过了设定的限流阈值。如果超过了阈值，就会触发限流操作，比如拒绝新的请求或者将请求放入队列等待处理。
5. 滑动时间窗口：在检查并可能触发限流操作后，时间窗口会向右滑动一个间隔，同时最旧的一个时间间隔的计数会被清空，为新的请求腾出空间。
6. 重复上述步骤：然后重复接收请求、检查并触发限流操作以及滑动时间窗口的步骤，以持续监控并处理网络流量。

通过这种方式，滑动时间窗算法能够在任意时间窗口内应对突然的流量暴增，避免了固定时间窗算法可能导致的服务雪崩问题。同时，通过调整时间窗口的大小和滑动间隔，可以实现更精细的流量控制。

下面是一个使用Java代码简单实现滑动时间窗算法的示例。在这个示例中，我们将使用LinkedList来模拟滑动窗口，并使用HashMap来记录每个请求的时间戳。

**每次请求都滑动窗口**

```java
import java.util.*;

public class SlidingTimeWindow {
    // 存储请求的队列，队列中的元素是请求的时间戳
    private LinkedList<Long> requestQueue = new LinkedList<>();
    // 存储每个请求的时间戳，key是请求的唯一标识，value是请求的时间戳
    private HashMap<String, Long> requestTimeMap = new HashMap<>();
    // 时间窗口的大小，单位是毫秒
    private long timeWindow = 60 * 1000; // 1分钟
    // 限流阈值
    private int limitThreshold = 100;

    /**接收并处理请求*/
    public void addRequest(String requestId, long currentTimeMillis) {
        // 检查并可能触发限流操作
        if (shouldLimit(currentTimeMillis)) {
            System.out.println("限流触发，拒绝请求：" + requestId);
            return;
        }
        
        // 添加请求到队列和哈希表
        requestQueue.addLast(currentTimeMillis);
        requestTimeMap.put(requestId, currentTimeMillis);
        
        // 滑动时间窗口
        slideWindow(currentTimeMillis);
        
        System.out.println("请求成功：" + requestId);
    }

    /**
     * 判断是否需要限流
     * 当requestQueue中距离当前请求时间小于timeWindow(也就是说处于同一时间窗口)的请求数大于limitThreshold(限流阈值)时就拒绝或丢弃请求
     */
    private boolean shouldLimit(long currentTimeMillis) {
        // 检查时间窗口内的请求数量是否超过阈值
        int count = 0;
        for (Long timestamp : requestQueue) {
            if (currentTimeMillis - timestamp <= timeWindow) {
                count++;
            } else {
                break;
            }
        }
        return count > limitThreshold;
    }

    /**
     * 判断是否需要滑动窗口
     * 此例子未用到slideInterval变量,是每次请求都来判断是否滑动
     * 删除于当前请求时间相差大于timeWindow(已经不处于同一窗口了)的请求
     */
    private void slideWindow(long currentTimeMillis) {
        // 检查是否需要滑动窗口
        if (currentTimeMillis - requestQueue.getFirst() > timeWindow) {
            // 移除时间窗口外的请求
            while (currentTimeMillis - requestQueue.getFirst() > timeWindow) {
                Long timestamp = requestQueue.removeFirst();
                String requestId = findRequestIdByTimestamp(timestamp);
                requestTimeMap.remove(requestId);
            }
        }
    }

    private String findRequestIdByTimestamp(Long timestamp) {
        for (Map.Entry<String, Long> entry : requestTimeMap.entrySet()) {
            if (entry.getValue().equals(timestamp)) {
                return entry.getKey();
            }
        }
        return null;
    }
}
```
这个示例中的`SlidingTimeWindow`类有一个`addRequest`方法，用于接收和处理请求。在接收到请求时，首先会检查是否需要触发限流操作，如果需要，则拒绝请求；否则，将请求添加到队列和哈希表中，并滑动时间窗口。`shouldLimit`方法用于检查时间窗口内的请求数量是否超过阈值，`slideWindow`方法用于滑动时间窗口并移除时间窗口外的请求。`findRequestIdByTimestamp`方法用于根据时间戳找到对应的请求的唯一标识。

###### 固定时间滑动窗口
```java
import java.util.*;  
  
public class SlidingTimeWindow {  
    private LinkedList<Long> requestQueue = new LinkedList<>();  
    private HashMap<String, Long> requestTimeMap = new HashMap<>();  
    private long timeWindow = 60 * 1000; // 1分钟  
    private long slideInterval = 10 * 1000; // 10秒  
    private int limitThreshold = 100;  
    private long lastSlideTime = 0; // 上次滑动时间窗口的时间戳  
  
    public void addRequest(String requestId, long currentTimeMillis) {  
        // 检查是否需要滑动时间窗口  
        if (currentTimeMillis - lastSlideTime >= slideInterval) {  
            slideWindow(currentTimeMillis);  
            lastSlideTime = currentTimeMillis;  
        }  
          
        // 检查并可能触发限流操作  
        if (shouldLimit(currentTimeMillis)) {  
            System.out.println("限流触发，拒绝请求：" + requestId);  
            return;  
        }  
          
        // 添加请求到队列和哈希表  
        requestQueue.addLast(currentTimeMillis);  
        requestTimeMap.put(requestId, currentTimeMillis);  
          
        System.out.println("请求成功：" + requestId);  
    }  
  
    private boolean shouldLimit(long currentTimeMillis) {  
        int count = 0;  
        for (Long timestamp : requestQueue) {  
            if (currentTimeMillis - timestamp <= timeWindow) {  
                count++;  
            } else {  
                break;  
            }  
        }  
        return count > limitThreshold;  
    }  
  
    private void slideWindow(long currentTimeMillis) {  
        // 移除时间窗口外的请求  
        while (currentTimeMillis - requestQueue.getFirst() > timeWindow) {  
            Long timestamp = requestQueue.removeFirst();  
            String requestId = findRequestIdByTimestamp(timestamp);  
            requestTimeMap.remove(requestId);  
        }  
    }  
  
    private String findRequestIdByTimestamp(Long timestamp) {  
        for (Map.Entry<String, Long> entry : requestTimeMap.entrySet()) {  
            if (entry.getValue().equals(timestamp)) {  
                return entry.getKey();  
            }  
        }  
        return null;  
    }  
}
```
在这个修改后的示例中，我们添加了一个lastSlideTime变量来记录上次滑动时间窗口的时间戳。在每次接收到请求时，我们会检查当前时间与上次滑动时间的时间差是否大于等于滑动间隔，如果是，则滑动时间窗口并更新lastSlideTime的值。这样可以确保在每个滑动间隔结束时才会滑动时间窗口。

