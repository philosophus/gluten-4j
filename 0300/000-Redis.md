# Redis数据类型与数据结构的对应

- 字符串 String：SDS(`Simple Dynamic String`)简单动态字符串。

- 列表 List：双向链表&压缩列表(ziplist)。**新版本改成 快速列表(V3.2新增结构`qulcklist`)**

- 哈希 Hash：哈希表(`HashTable`)&压缩列表。**新版本改成 哈希表&紧凑列表(V5新增结构`listpack`)**

- 集合 Set：哈希表&整数集合(`IntSet`)

- 有序集合 Zset：跳表(`skiplist`)&压缩列表。**版本改成 跳表(`skiplist`)&紧凑列表(`listpack`)**

## SDS(`Simple Dynamic String`)简单动态字符串

Redis是由C语言实现的，但是其没有用C传统的字符串表示，而是自己构建了一种名为**简单动态字符串(Simple Dynamic String) SDS** 的抽象类型来最为Redis的默认字符串标识。

### 3.2以前 结构

```C
struct sdshdr{
    //记录buf数组中已使用的字节数----等于SDS保存的字符串长度
    unsigned int len;
    //记录buf数组中未使用的字节数
    unsigned int free;
    //保存字符串
    char buf[];
}
```

Len和free都用的int，比较浪费空间。  

### 3.2以后版本 结构

属性有所变化

- len：已使用的字符串长度
- alloc：数组总长度
- flags：SDS类型
- buf[]：字节数组

#### SDS五种类型

| **SDS类型** | **C类型** | **len&alloc类型** | **长度**             | **Flags** |
| --------- | ------- | --------------- | ------------------ | --------- |
| **SDS5**  |         |                 | 1<<5  2^5  32      |           |
| **SDS8**  | char    | uint8_t         | 1<<8  2^8  256     |           |
| **SDS16** | short   | Uint16_t        | 1<<16  2^16  65535 |           |
| **SDS32** | int     | Uint32_t        | 1<<32  2^32        |           |
| **SDS64** | long    | Uint64_t        | 1<<64  2^64        |           |

#### flags解释

是char类型，一个字节，8位。定义如下

- define SDS_TYPE_5  0 
- define SDS_TYPE_8  1
- define SDS_TYPE_16 2
- define SDS_TYPE_32 3
- define SDS_TYPE_64 4

由于一共有5种类型的结构，所以至少要3位来区分(2^ 3=8>5)

### 结构解释

#### SDS5

```C
struct _attribute_((_paked_)) sdshdr5{
    unsigned char flags;//低三位表示类型；高五位表示字符串长度
    char bug[];
}
```

**对于长度小于32位的字符串，redis设计的数据结构中为了节省存储空间，并没有设置len和alloc字段。
flags剩下的5位(2^5=32)用来表示字符串的长度。**

#### SDS8、16、32、64

N对应SDS类型8，16，32，64

```c
struct _attribute_((_paked_)) sdshdrN{
    uintN_t len;//已使用长度
    uintN_t alloc;//数组总长度
    unsigned char flags;//第三位表示类型；高五位未使用(预留空间)
    char bug[];
}
```

### 扩容

当SDS中剩余空间(avail)小于或等于新增内容长度(addlen)时：

- 新增后总长度(len+addlen)<1M：按新长度两倍扩容。
- 新增后总长度(len+addlen)>1M：按新长度加1M扩容。

```新长度：len + addlen后的长度。```         

#### 空间预分配策略

由于预先分配了更多的空间，如果再加入数据，还能放得下，就不用再做扩容操作，从而减少空间分配次数提高性能。

#### 惰性空间释放

如果SDS内容变少了，不会马上回收多余空间，只会更新len长度。调用sdsRemoveFreeSpace()方法会回收SDS字符串的空白空间，将free设置为0或更新alloc(取决于版本)

### 与C字符串对比

- **<mark>C字符串二进制不安全</mark>**。C使用N+1长度的字符串数组表示N长度的字符串，最后一个元素总是空字串‘\0’。(C语言\0表示空字串)，如果存二进制，到0会被截断；**<mark>SDS二进制安全</mark>**，根据len取字符串

- ***<mark>C时间复杂度为O(n)</mark>***。字符串不记录自身长度。获取长度需要遍历整个字符串，直到遇到‘\0’；**<mark>SDS时间复杂度为O(1)</mark>**，len属性记录了字串长度。

- C会出现缓冲区溢出导致覆盖内存里相邻的内容；SDS会自动扩容

- SDS减少修改字符串时带来的内存重分配次数

- SDS兼容部分C字符串函数
  
  ![](assets/2023-12-08-14-40-41-image.png)

### 源码

  [redis基础数据结构（1）-SDS源码解析 - 简书 (jianshu.com)](https://www.jianshu.com/p/68e7361979ac)

## 其他数据结构未整理，推荐两个

- [redis源码分解二(上层容器合集)](https://zhuanlan.zhihu.com/p/457755105)
- [redis源码分解一(底层数据结构合集)](https://zhuanlan.zhihu.com/p/426907206)
- [Redis数据结构](https://blog.csdn.net/mz474920631/article/details/125200050)

# 基本类型

### String字符串

二进制安全的，意思是string 可以包含任何数据。最大512M

### List双向链表

string类型双向链表，有序，且可以根据范围筛选数据，提升响应速度。**<mark>可以实现排队，数据分页</mark>**

### Set无序集合

通过hash表实现string类型无序集合，去重。**<mark>可以做标记，如收藏，点赞等。</mark>**

### ZSet有序集合

通过hash表实现string类型有序集合，每个元素有一个分数(score)，通过分数排序，分数可重复，元素不得重复。**<mark>score作为时间戳可以实现延时队列、滑动窗口限流； score可以做排行。</mark>**

### Hash键值对

键值对存储。适合存对象，比string省空间；修改对象某一值时不需要拿出整条数据反序列化，节省开销。    

### 特殊类型

#### HyperLogLogs基数统计

此结构可以非常省内存的去统计各种计数，比如注册 IP 数、每日访问 IP 数的页面实时UV、在线用户数，共同好友数等。

#### Bitmap位存储

统计用户信息，活跃，不活跃！ 登录，未登录！ 打卡，不打卡！ 两个状态的，都可以使用 Bitmaps。有是否、有无概念的都可以用，特别省空间。

#### grospatial地理位置

底层的实现原是Zset。可以推算地理位置的信息: 两地之间的距离, 方圆几里的人。

# 底层结构

- RedisServer 里面有个 `redisDb *db` 数组，该数组中有默认16个 redisDb 。

- redisDb 中有个 dict *dict 键空间(字典)。字典的 dictEntry 就是具体的redis数据，key为redis键，value是一个redisObject。

- redisObject存储了：对象的类型(上层容器)，字符串/列表/集合/哈希表、底层的数据结构、LRU 时间、引用计数、真实数据的指针等信息，加上留空的两位，一共32位

![](assets/2023-12-08-14-43-39-image.png)

## redisServer、redisDb、dict、dictEntry、redisObject介绍

### redisServer 结构保存服务器的状态

- Redis 服务器使用 redisServer 结构保存服务器的状态。记录着 服务器的数据库数量 和 redisDb 类型的数组保存着所有的数据

```c
struct redisServer {
    //服务器的数据库数量
    int dbnum;
    //一个数组，保存所有数据库对象
    redisDb *db;
}
```

- Redis 是一个键值对 (key-value pair) 数据库服务器，服务器中的每个数据库都由一个 redisDb 结构表示，其中，redisDb 结构的 **dict 字典**保存了数据库中的所有键值对，即**键空间**。

### redisDb 结构，表示 Redis 数据库的结构

```c
typedef struct redisDb {

    dict *dict;//保存了当前数据库的键空间

    dict *expires;//键空间中所有键的过期时间

    int id;//Database ID  从0开始

} redisDb;
```

### dict(字典)结构、键空间简介

redis中的dict(字典)是一个安全的实现了rehash的多态哈希桶，是一个性能较高的数据结构，用来保存键值对，类似其他高级语言的map。

- 键空间键：就是redis的key，每一个键都是一个字符串对象，而且唯一

- 键空间值：是数据库的值，每一个值都是一个redisObject，可以是Redis的各种类型(字符串、列表、集合、有序集合、哈希)。

*推荐个详细的*：[redis源码学习-dict篇](https://zhuanlan.zhihu.com/p/421128698)

### redisObject结构，值就保存于此

redis每个对象(key-value的value)都由一个redisObject的结构表示:

```c
//刚刚好32 bits

typedef struct redisObject {

    unsigned type:4;// 对象的类型(上层容器)，字符串/列表/集合/哈希表

    unsigned notused:2;// 对齐位，未使用的两个位

    unsigned encoding:4;// 编码方式，也就数据结构

    unsigned lru:22;//// LRU 时间(相对于 server.lruclock)，当内存紧张，淘汰数据的时候用到

    int refcount;// 引用计数

    void *ptr;// 数据指针，指向对象的值

}
```

# 命令声明周期

##### 命令处理流程图

![](assets/2023-12-08-14-45-31-image.png)

##### 详细流程

1. client与server建立连接

2. `readQueryFromClient()`函数解析请求命令：该方法会从socket中读取数据放到输入缓冲区querybuf中，接着会调用processInputBuffer()方法按照Redis采用自定义的RESP协议来解析参数。

3. `processCommand()`函数处理具体的命令：
   
   - 解析完参数之后会调用processCommand()方法执行具体的命令。
   - 在处理命令请求之前会有很多校验，如：*是否是quit命令；命令是否存在；参数数目是否合法；客户端是否认证通过；内存限制校验、集群相关校验、持久化相关校验、主从复制相关校验、发布订阅相关校验及事务操作等*
   - 根据命令名称找到对应的命令并调用命令的call()完成具体的操作

4. `addReply()`方法返回执行结果：命令在执行完成之后都会调用`addReply()`方法返回执行结果。
   
   - Redis服务器根据不同的返回类型选择不同的协议格式 ，客户端根据返回结果的第一个字符判断返回类型。
     
     ```
     状态回复，第一个字符是"+"；
     
     错误回复，第一个字符是"-"；
     
     整数回复，第一个字符是":"；
     
     批量回复，第一个字符是"$"；
     
     多条批量回复，第一个字符是"*"；
     ```
   
   - addReply()方法只是把返回的数据写入到输出缓冲区`client->buf` 或 输出链表`client->reply`中(*通常缓存数据时都会先尝试缓存到buf输出缓冲区，如果失败会再次尝试缓存到reply输出链表*)，并不执行实际的网络发送操作。

5. 发送数据给客户端：实际的网络发送数据操作在beforeSleep()方法中完成。
   
   - beforeSleep()会在handleClientsWithPendingWrites()/handleClientsWithPendingWritesUsingThread()中遍历clients_pending_write链表中每一个客户端节点，并调用writeToClient()方法把输出缓冲区client->buf和client->reply中的数据通过socket发送给客户端。
   
   - 当返回结果数据量非常大时，writeToClient()执行之后，客户端输出缓冲区或者输出链表中可能还有部分数据未发送给客户端。这是就需要 监听当前客户端socket文件描述符的可写事件，当客户端可写时，sendReplyToClient()会发送剩余部分的数据给客户端。

*其他人整理的版本*：[redis之命令请求执行过程](https://blog.csdn.net/yang1018679/article/details/115430029)

## 读写键空间的维护操作

- 读取一个键之后，服务器会根据键是否存在来更新服务器的键空间命中 (hit) 次数或键空间不命中 (miss) 次数。还会更新键的 LRU 时间，该值可以用于计算键的闲置时间。

- 如果服务器读取一个键时发现该键已经过期，那么会先删除这个过期键，然后才执行其他操作。

- 服务器每次修改一个键之后，都会对脏键(dirty)计数器的值增 1，这个计数器会触发服务器的持久化以及复制操作。

- 如果有客户端使用 WATCH 命令监视了某个键，那么服务器在对被监视的键进行修改之后，会将这个键标记为脏 (dirty)，从而让事务程序注意到这个键已经被修改过。

- 如果服务器开启了数据库通知功能，那么在对键进行修改之后，服务器将按配置发送相应的数据库通知。

# 事务

redis事务的本质是一组命令的集合。事务下所有命令都会被序列化，一次性、顺序性、排他性的执行。    

[Redis事务 - SunnyBigBoy - 博客园 (cnblogs.com)](https://www.cnblogs.com/SunnyBigBoy/p/14770547.html)

**实现方式**  

multi开启事务，一系列命令入队，exec执行事务。

**事务回滚(终止)**  

- discard命令放弃事务，一系列命令都放弃执行。

- 编译异常(一系列命令中代码有误)，事务中的命令都不会执行    

- 运行时异常(某些指令运行时出错)，错误的指令抛出异常，其他指令会正确执行。

# 分布式锁

[分布式锁 Redisson Redlock - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/440865954)

[Redis在项目中的应用 - 知乎(zhihu.com)](https://zhuanlan.zhihu.com/p/92064192)

[RedLock-红锁 - 简书 (jianshu.com)](https://www.jianshu.com/p/8d929ea3c5c6)

## 分布式锁需要具备的特性

- **互斥性**: 任意时刻，只有一个客户端能持有锁。

- **锁超时释放**：持有锁超时，可以释放，防止不必要的资源浪费，也可以防止死锁。

- **可重入性**:一个线程如果获取了锁之后,可以再次对其请求加锁。

- **高性能和高可用**：加锁和解锁需要开销尽可能低，同时也要保证高可用，避免分布式锁失效。

- **安全性**：锁只能被持有的客户端删除，不能被其他客户端删除

## Redis实现分布式锁方案有哪些？

- SETNX + EXPIRE

- SETNX + value(系统时间+过期时间)

- 使用Lua脚本(包含SETNX + EXPIRE两条指令)

- SET的扩展命令(SET EX PX NX)

- SET EX PX NX  + 校验唯一随机值,再释放锁

**这几种方案都有一些问题。不具备原子性、没有保存持有者的唯一标识、存在业务未执行完，锁却到期释放等问题。**

## Redisson RLock

### Redisson RLock简介、可重入锁解释

Redisson的watch dog(看门狗)机制解决了**业务未执行完，锁却到期释放的问题**。*Redission 基于高性能异步无锁Java Redis客户端和Netty框架。*

redisson 基于redis集群模式实现的分布式可重入锁是使用的**hash数据结构**，key是自定义的，map key是客户端某个线程的唯一标识，map value就是重入次数。**使用的lua脚本操作 加锁、重置失效时间、解锁**

**可重入锁**：也叫做递归锁。指的是同一线程外层函数获得锁之后 ，内层递归函数仍然可以获取该锁，但不受影响。

### 加锁流程

- 线程获取锁成功则执行lua脚本，保存数据到redis。

- 如果获取失败: 一直通过while循环尝试获取锁(可自定义等待时间，超时后返回失败)，获取成功后，执行lua脚本，保存数据到redis数据库。

- Redisson提供的分布式锁支持锁自动续期，如果线程业务没有执行完，那么redisson会自动给redis中的目标key延长超时时间，这就是**Watch Dog 机制**

### Wath Dog的自动延期机制

- 线程加锁成功后，就会启动一个watch dog看门狗，它是一个后台线程，是业务线程的守护线程。

- rlock的超时时间默认时30秒，看门狗会每隔10秒(超时时间/3)检查一下，如果线程还持有锁，就延长锁生存时间(恢复到30秒)。

- 看门狗的续期时间可以通过修改Config.lockWatchdogTimeout来另行指定。

### 详细加锁(lock)原理(Redis luster模式)

1. 加锁、重入锁
   
   1. 加锁的时候会根据的锁名字(`redissonClient.getLock("Lock_Name")`)，计算出来属于redis集群中的哪个槽(redis集群槽16384个)。
   
   2. 然后从集群中找出这个槽所在的master机器，接着就是根据clientId(创建客户端的时候的一个UUID)拼接上线程id生成一个key。
   
   3. 接着使用一段lua脚本加锁，内部是使用的hash结构，key是锁名字，然后对应的map key就是clientId+线程id，map value就是1,这个1是与可重入有关的，还会设置过期时间，默认是30s。
   
   4. 如果锁不存在(`exists Lock_Name == 0`)。就会执行`hset Lock_Name clientId+线程id 1` 和`pexpire Lock_Name 30000ms`这两个命令，最后返回null，首次加锁逻辑就是这样的。
   
   5. 如果锁存在，就自增1，重置一下过期时间是30s，最后return null。**这就是重入机制(可重入锁)**
      
      ```lua
      --KEYS[1] 锁名字，也就是"Lock_Name"
      --ARGV[2] clientId+线程id
      --ARGV[1] 是过期时间,默认是30s
      --如果锁不存在
      if (redis.call('exists', KEYS[1]) == 0) then 
        redis.call('hset', KEYS[1], ARGV[2], 1); 
        redis.call('pexpire', KEYS[1], ARGV[1]);
        return nil;
      end;
      --如果锁存在
      if (redis.call('hexists', KEYS[1], ARGV[2]) == 1) then
        redis.call('hincrby', KEYS[1], ARGV[2], 1);
        redis.call('pexpire', KEYS[1], ARGV[1]);
        return nil;
      end;
      return redis.call('pttl', KEYS[1]);
      ```

2. 创建定时任务(watch dog 看门狗)
   
   ```lua
   --如果这个锁还存在，就重置一下过期时间，然后return 1。
   if (redis.call('hexists', KEYS[1], ARGV[2]) == 1) then
       redis.call('pexpire', KEYS[1], ARGV[1]);
       return 1;
   end;
   return 0;
   ```
   
   - 加锁成功之后，会创建一个定时任务(watch dog 看门狗)，每10s(自定义过期时间/3)执行一次，不断为这个锁续期，看看如果这个锁还存在的话，就重置一下过期时间为30s(自定义过期时间)。

3. **互斥机制**，其他线程自旋，等待锁
   
   - 这个时候其他线程或者是其他客户端尝试获取这个redis分布式锁，就会失败，然后就会返回一个ttl，这个ttl就是过期时间(`return redis.call('pttl', KEYS[1])`)。它就会循环等待这个ttl时间过后，然后再尝试加下锁，如果不行再返回ttl，这个时候再等待，就是这样一直循环。

### 详细解锁(unlock)原理(Redis luster模式)

4. 跟加锁一样，根据的锁名字(`redissonClient.getLock("Lock_Name")`)，计算出来属于redis集群中的哪个槽

5. 然后从集群中找出这个槽所在的master，根据clientId+线程id作为key，使用lua脚本解锁。

6. **如果map的值大于0**，说明加锁不止一次，也就是加了重入锁，这个时候就会重置一下过期时间。**如果小于等于0**，就说明这个锁要释放了，这个时候就会删除这个key，并发布一个锁销毁的通知给那些其他未获取到锁的线程订阅者(`publish`命令)。

7. 解锁成功后销毁定时任务(watch dog)
   
   ```lua
   --KEYS[1]是锁名字
   --KEYS[2]是 redisson_lock__channel:{锁名字} 这么一个东西，他其实也是个key，可以理解为主题， 发布订阅用的
   --ARGV[1]是解锁的标识符
   --ARGV[2] 过期时间
   --ARGV[3] clientId+线程Id
   if (redis.call('exists', KEYS[1]) == 0) then
       redis.call('publish', KEYS[2], ARGV[1]);
    return 1;
   end;
   if (redis.call('hexists', KEYS[1], ARGV[3]) == 0) then
    return nil;
   end;
   local counter = redis.call('hincrby', KEYS[1], ARGV[3], -1);
   if (counter > 0) then
    redis.call('pexpire', KEYS[1], ARGV[2]);
    return 0;
   else 
    redis.call('del', KEYS[1]);
    redis.call('publish', KEYS[2], ARGV[1]);
    return 1;
   end; 
   return nil;
   ```

### Java代码

```xml
<dependency>
 <groupId>org.redisson</groupId>
 <artifactId>redisson-spring-boot-starter</artifactId>
 <version>3.9.1</version>
</dependency>
```

```java
public void fun() throws Exception{
 RLock lock = redissonClient.getLock("redisKey");// 拿锁失败时会不停的重试
 // 具有Watch Dog 自动延期机制 默认续30s 每隔30/3=10 秒续到30s
 lock.lock();
 // 尝试拿锁10s后停止重试,返回false 具有Watch Dog 自动延期机制 默认续30s
 boolean res1 = lock.tryLock(10, TimeUnit.SECONDS); 
 // 没有Watch Dog ，10s后自动释放
 lock.lock(10, TimeUnit.SECONDS);
 // 尝试拿锁100s后停止重试,返回false 没有Watch Dog ，10s后自动释放
 boolean res2 = lock.tryLock(100, 10, TimeUnit.SECONDS);
 lock.unlock();
}
```

### Redisson锁的缺点：主从同步、锁遇到故障转移

在极端情况下

客户端A加锁成功后，master节点数据会异步复制到slave节点，此时当前持有Redis锁的master节点宕机，slave节点被提升为新的master节点；

客户端B再次加锁，在新的master节点上加锁也也会成功，这个时候客户端B也会认为加锁成功，出现两个节点同时持有一把锁的问题；

就出现脏数据，**丧失了互斥性**。

解决这个问题，就用到RedLock算法。

## RedLock算法(Redis Distributed Lock)

### 概念

普通的redis分布式锁，是在redis集群中根据hash算法选择一台redis实例创建一个锁就可以了

RedLock算法思想，不能只在一个redis实例上创建锁，应该是在多个redis实例上创建锁(`n / 2 + 1`)，必须在大多数redis节点上都成功创建锁，才能算这个整体的RedLock加锁成功，避免说仅仅在一个redis实例上加锁。从而避免**A加锁成功后master节点宕机导致B成功加锁到新的master节点上**的问题

### RedLock缺点、存在的问题、RedLock已被弃用

- 红锁是很少使用的。这是因为使用了红锁后会影响高并发环境下的性能。
- 使用红锁时，需要提供多套Redis的主从部署架构，同时，这多套Redis主从架构中的Master节点必须都是独立的，相互之间没有任何数据交互。
- 使用红锁后，当加锁成功的RLock个数不超过总数的一半时，会返回加锁失败，即使在业务层面任务加锁成功了，但是红锁也会返回加锁失败的结果。
  **实际场景中，一般都是要保证Redis集群的可靠性，而不使用RedLock算法实现的分布式锁**

**Redisson RedLock 是基于联锁 MultiLock 实现的，但是使用过程中需要自己判断 key 落在哪个节点上，对使用者不是很友好。Redisson RedLock 已经被弃用。直接使用普通的加锁，会基于 wait 机制将锁同步到从节点，不能保证绝对的一致性。是最大限度的保证一致性。**

### 实现逻辑(简版)

场景是假设有一个redis cluster，有3个redis master实例。然后获取分布式锁：

1. 获取当前时间戳，单位是毫秒
2. 按顺序向每个master节点请求加锁。客户端设置网络连接和响应超时时间，并且超时时间要小于锁的失效时间(假设锁自动失效时间为10秒，则超时时间一般在5-50毫秒之间,假设超时时间是50ms)。如果超时，跳过该master节点，尽快去尝试下一个master节点。
3. 客户端使用当前时间减去开始获取锁时间(即步骤1记录的时间)，得到获取锁使用的时间。当且仅当超过一半（N/2+1，这里是5/2+1=3个节点）的Redis master节点都获得锁，并且使用的时间小于锁失效时间时，锁才算获取成功。(10s > 30ms+40ms+50ms+4m0s+50ms)
   **注意事项：**
- 如果取到了锁，key的真正有效时间就变啦，需要减去获取锁所使用的时间。
- 如果获取锁失败(没有在至少N/2+1个master实例取到锁，有或者获取锁时间已经超过了有效时间)，客户端要在所有的master节点上解锁(即便有些master节点根本就没有加锁成功，也需要解锁，以防止漏掉)。
- 线程A创建了一把分布式锁，线程B就得不断轮询去尝试获取锁。

## 不常用的几种分布式锁方案

### SETNX + EXPIRE(不是原子操作，死锁风险)

setnx和expire两个命令分开了，「不是原子操作」。如果执行完setnx加锁，expire命令设置超时时间时失败，**发生异常锁得不到释放**，会造成死锁

### SETNX + value(系统时间+过期时间)

为了解决**发生异常锁得不到释放**的场景，过期时间客户端生成，把**过期时间放到setnx的value值**里面。

*存在问题*：

- 多客户端时间必须同步，但是时间流速不一定相同。

- 锁过期时间可能被其他客户端修改。

- 锁没有保存持有者的唯一标识，可能被别的客户端释放/解锁。

### 使用Lua脚本(包含SETNX + EXPIRE两条指令)

  Lua脚本来保证setnx和expire两条指令的原子性。

### SET的扩展命令(SET EX PX NX)

`SET key value[EX seconds][PX milliseconds][NX|XX]`也是原子性的。

- NX :表示key不存在的时候，才能set成功，保证只有第一个客户端请求才能获得锁，而其他客户端请求只能等其释放锁，才能获取。

- EX seconds :设定key的过期时间，时间单位是秒。

- PX milliseconds: 设定key的过期时间，单位为毫秒

- XX: 仅当key存在时设置值

*存在问题*：锁没有保存持有者的唯一标识

### SET EX PX NX  + 校验唯一随机值,再释放锁

给value值设置一个标记当前线程唯一的随机数，在删除的时候，校验一下。

用lua脚本执行**判断是不是当前线程加的锁** 和 **释放锁**两个操作保证原子性。

# 持久化

##### 为什么要持久化

数据在内存中容易丢，如果每次都重新从关系库去恢复，对关系库压力较大。

## 持久化方案

[Redis 持久化之 RDB 与 AOF 详解 (baidu.com)](https://baijiahao.baidu.com/s?id=1682713097817867905&wfr=spider&for=pc)  

[Redis的持久化（重要，面试题） - 百度文库 (baidu.com)](https://wenku.baidu.com/view/d889715fa16925c52cc58bd63186bceb19e8ed08.html?_wkts_=1702019462713)  

redis重启时会自动从快照中恢复数据，redis提供两种持久化方案：**RDB(Redis DataBase)和AOF(Append Only File)**，<mark>默认使用RDB</mark>。    
同时使用AOF和RDB时。重启时，会优先使用AOF文件去恢复原始数据。因为AOF中保存的数据通常比RDB中保存的数据更加完整。  

### RDB(Redis DataBase)

[四、Redis数据快速恢复之RDB(Redis DataBase) - 简书 (jianshu.com)](https://www.jianshu.com/p/f5c0ddb84be4)  

[纯干货｜深度解析Redis持久化策略 (baidu.com)](https://baijiahao.baidu.com/s?id=1676846338717439440&wfr=spider&for=pc)  

<mark>指定时间内key的改变达到指定数量时，就自动快照。</mark>

### AOF(Append Only File)

[第八章 Redis数据持久化之AOF - 年少纵马且长歌 - 博客园 (cnblogs.com)](https://www.cnblogs.com/jhno1/p/15634316.html)  

<mark>记录用户的每次操作。每次操作追加到.aof文件里。文件大小超过重写策略或手动重写时，会对AOF文件rewrite重写，压缩AOF文件容量。</mark>  

# 高可用

三种集群：主从、哨兵(Sentinel)、Cluster      

[Redis的三种集群方式 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/504698662)   

### 主从

### 哨兵(Sentinel)

### 集群模式(Cluster)

[Redis集群模式2-RedisCluster模式 - 简书 (jianshu.com)](https://www.jianshu.com/p/80c33a56233d)   

### 脑裂问题

**概念解释**  

精神分裂，多个主节点。一般只出现在主从模式，哨兵也只是加强的主从

**触发原因**  

- 哨兵与master失去连接但客户端可以访问，哨兵重新选举了新的master，如此，对于客户端来说就有两个master。

- master失去连接(阻塞或各种原因)，哨兵选举的时候master又通了。

**引起问题**  

多个master写入数据导致数据不一致，如果此时其中一个master重新定义为slave就回导致部分数据丢失。  

**解决方案**  

多数派共识算法。    
slaveRedis 提供了两个配置项来限制主库的请求处理。    

- min-slaves-to-write(新版本min-replicas-to-write)：这个配置项设置了主库能进行数据同步的最少从库数量；

- min-slaves-max-lag(新版本min-replicas-max-lag)：这个配置项设置了主从库间进行数据复制时，从库给主库发送 ACK 消息的最大延迟（以秒为单位）。    

此二配置项组合后的效果：主库连接的从库中至少有 N 个从库，和主库进行数据复制时的 ACK 消息延迟不能超过 T 秒，否则，主库就不会再接收客户端的请求了。    

**<mark>官方言：redis并不能保证强一致性。</mark>**所以极端情况下还是可能数据不一致，得人工干预。

# 数据一致性

[Redis和数据库的数据一致性问题 - 蝉沐风 - 博客园 (cnblogs.com)](https://www.cnblogs.com/chanmufeng/p/15894471.html)  

[技能提升(1）-- 缓存专题 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/612304385)   

# 过期策略

Redis有定期删除和惰性删除两种策略。**定期删除是集中处理，惰性删除是零散处理。**

## 定期删除

Redis 会将每个设置了过期时间的 key 放入到一个独立的字典中，以后会定期遍历这个字典来删除到期的 key。

Redis 默认会每秒进行十次过期扫描(100ms一次`redis.conf中有一个属性"hz"，默认为10`)，过期扫描不会遍历过期字典中所有的 key，而是采用了一种简单的贪心策略。

1. 从过期字典中随机 20 个 key；
2. 删除这 20 个 key 中已经过期的 key；
3. 如果过期的 key 比率超过 1/4，那就重复步骤 1，直到可能过期的key百分比低于25%。也就是任何给定时刻，使用内存的已过期key的最大数量不超过每秒最大写入操作数量除以 4。

redis默认是每隔 100ms就随机抽取一些设置了过期时间的key，检查其是否过期，如果过期就删除。

### 为什么要随机呢？

假如 redis 存了几十万个 key ，每隔100ms就遍历所有的设置过期时间的 key 的话，就会给 CPU 带来很大的负载。

## 惰性删除

客户端访问这个key的时候，redis对key的过期时间进行检查，如果过期了就立即删除，不会返回任何东西。

定期删除可能会导致很多过期key到了时间并没有被删除掉。所以就有了惰性删除。

# 淘汰策略

## 为什么需要淘汰策略？

定期删除和惰性删除都不是一种完全精准的删除，所以就需要内存淘汰策略进行补充。

## 8种淘汰策略

1. noeviction：当内存使用超过配置的时候会返回错误，不会驱逐任何键
2. allkeys-lru：加入键的时候，如果过限，首先通过LRU算法驱逐最久没有使用的键
3. volatile-lru：加入键的时候如果过限，首先从设置了过期时间的键集合中驱逐最久没有使用的键
4. allkeys-random：加入键的时候如果过限，从所有key随机删除
5. volatile-random：加入键的时候如果过限，从过期键的集合中随机驱逐
6. volatile-ttl：从配置了过期时间的键中驱逐马上就要过期的键
7. volatile-lfu：从所有配置了过期时间的键中驱逐使用频率最少的键
8. allkeys-lfu：从所有键中驱逐使用频率最少的键

# Redis的LRU算法实现(近似LRU)

   Redis维护了一个24位时钟，可以简单理解为当前系统的时间戳，每隔一定时间会更新这个时钟。每个key对象内部同样维护了一个24位的时钟，当新增key对象的时候会把系统的时钟赋值到这个内部对象时钟。比如我现在要进行LRU，那么首先拿到当前的全局时钟，然后再找到内部时钟与全局时钟距离时间最久的（差最大）进行淘汰，这里值得注意的是全局时钟只有24位，按秒为单位来表示才能存储194天，所以可能会出现key的时钟大于全局时钟的情况，如果这种情况出现那么就两个相加而不是相减来求最久的key。

```C
struct redisServer {
 pid_t pid; 
 char *configfile; 
 //全局时钟
 unsigned lruclock:LRU_BITS; 
 ...
};
typedef struct redisObject {
 unsigned type:4;
 unsigned encoding:4;
 /* key对象内部时钟 */
 unsigned lru:LRU_BITS;
 int refcount;
 void *ptr;
} robj;
```

Redis中的LRU与常规的LRU实现并不相同，常规LRU会准确的淘汰掉队头的元素，但是Redis的LRU并不维护队列，只是根据配置的策略要么从所有的key中随机选择N个（N可以配置）要么从所有的设置了过期时间的key中选出N个键，然后再从这N个键中选出最久没有使用的一个key进行淘汰。

下图是常规LRU淘汰策略与Redis随机样本取一键淘汰策略的对比，浅灰色表示已经删除的键，深灰色表示没有被删除的键，绿色表示新加入的键，越往上表示键加入的时间越久。从图中可以看出，在redis 3中，设置样本数为10的时候能够很准确的淘汰掉最久没有使用的键，与常规LRU基本持平。

![](assets/2023-12-08-15-50-24-image.png)

## 为什么要使用近似LRU？

1. 性能问题，由于近似LRU算法只是最多随机采样N个key并对其进行排序，如果精准需要对所有key进行排序，这样近似LRU性能更高
2. 内存占用问题，redis对内存要求很高，会尽量降低内存使用率，如果是抽样排序可以有效降低内存的占用
3. 实际效果基本相等，如果请求符合长尾法则，那么真实LRU与Redis LRU之间表现基本无差异
4. 在近似情况下提供可自配置的取样率来提升精准度，例如通过 CONFIG SET maxmemory-samples指令可以设置取样数，取样数越高越精准，如果你的CPU和内存有足够，可以提高取样数看命中率来探测最佳的采样比例。

# LFU

   LFU是在Redis4.0后出现的，LRU的最近最少使用实际上并不精确，考虑下面的情况，如果在|处删除，那么A距离的时间最久，但实际上A的使用频率要比B频繁，所以合理的淘汰策略应该是淘汰B。LFU就是为应对这种情况而生的。

A--A--A--A--A--A--A--A--A--A----|

B-----B-----B-----B------------B|

LFU把原来的key对象的内部时钟的24位分成两部分，前16位还代表时钟，后8位代表一个计数器。16位的情况下如果还按照秒为单位就会导致不够用，所以一般这里以时钟为单位。而后8位表示当前key对象的访问频率，8位只能代表255，但是redis并没有采用线性上升的方式，而是通过一个复杂的公式，通过配置如下两个参数来调整数据的递增速度。

lfu-log-factor 可以调整计数器counter的增长速度，lfu-log-factor越大，counter增长的越慢。

lfu-decay-time 是一个以分钟为单位的数值，可以调整counter的减少速度。

所以这两个因素就对应到了LFU的Counter减少策略和增长策略。

# 其他常见问题
